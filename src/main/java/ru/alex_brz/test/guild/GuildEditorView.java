package ru.alex_brz.test.guild;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Route("guildEditor")
public class GuildEditorView extends AppLayout implements HasUrlParameter<Integer> {

    private Integer id;
    FormLayout contactForm;
    FormLayout buttonLayout;
    private TextField idHeadText;
    private TextField companyText;
    Button saveContact;
    Button cancel;

    @Autowired
    GuildRepository guildRepository;

    public GuildEditorView() {
        //Создаем объекты для формы
        contactForm = new FormLayout();
        buttonLayout = new FormLayout();
        idHeadText = new TextField(Guild.getCategories().get(1));
        companyText = new TextField(Guild.getCategories().get(2));
        saveContact = new Button("Сохранить");
        cancel = new Button("Отмена");

        saveContact.getElement().getThemeList().add("primary");
        //Добавим все элементы на форму
        buttonLayout.add(saveContact, cancel);
        contactForm.add(idHeadText, companyText);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(contactForm);
        verticalLayout.add(buttonLayout);
        setContent(verticalLayout);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer contactId) {
        id = contactId;
        if (!id.equals(0)) {
            addToNavbar(new H3("Редактировать цех"));
        } else {
            addToNavbar(new H3("Добавить цех"));
        }
        fillForm();
    }


    private void fillForm() {

        if (!id.equals(0)) {
            Optional<Guild> equipment = guildRepository.findById(id);
            equipment.ifPresent(x -> {
                idHeadText.setValue(String.valueOf(x.getIdHead()));
                companyText.setValue(x.getCompany());
            });
        }

        saveContact.addClickListener(clickEvent -> {
            save();
            notificationSave();
        });

        cancel.addClickListener(clickEvent -> {
            notificationCancel();
        });
    }

    private void save(){
        Guild guild = new Guild();
        if (!id.equals(0)) {
            guild.setId(id);
        }
        guild.setIdHead(Integer.parseInt(idHeadText.getValue()));
        guild.setCompany(companyText.getValue());
        guildRepository.save(guild);
    }
    private void notificationSave(){
        Notification notification = new Notification(id.equals(0) ?
                "Цех успешно создан" : "Цех был изменен", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(GuildListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationCancel(){
        Notification notification = new Notification("Отмена изменений", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(GuildListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
}
