package ru.alex_brz.test.test_engineer;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TestEngineerRepository extends CrudRepository<TestEngineer, Integer> {
    List<TestEngineer> findAll();

}