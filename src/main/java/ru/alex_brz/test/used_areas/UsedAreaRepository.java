package ru.alex_brz.test.used_areas;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsedAreaRepository extends CrudRepository<UsedArea, Integer> {
    List<UsedArea> findAll();

}