package ru.alex_brz.test.master;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MasterRepository extends CrudRepository<Master, Integer> {
    List<Master> findAll();
}