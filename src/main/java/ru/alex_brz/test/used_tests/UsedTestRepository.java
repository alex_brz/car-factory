package ru.alex_brz.test.used_tests;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsedTestRepository extends CrudRepository<UsedTest, Integer> {
    List<UsedTest> findAll();

}