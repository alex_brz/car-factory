package ru.alex_brz.test.test;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TestRepository extends CrudRepository<Test, Integer> {
    List<Test> findAll();
    /*@Query("select ...")//JPQL
    String getName();*/
}