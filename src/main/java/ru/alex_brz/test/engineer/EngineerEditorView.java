package ru.alex_brz.test.engineer;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.alex_brz.test.contact.ContactListView;

import java.util.Optional;

@Route("engineerEditor")
public class EngineerEditorView extends AppLayout implements HasUrlParameter<Integer> {

    Integer id;
    FormLayout mainForm;
    FormLayout buttonLayout;
    TextField name;
    TextField proffesion;
    Button saveItem;
    Button cancel;

    @Autowired
    EngineerRepository engineerRepository;

    /*Binder<Contact> binder = new Binder<>(Contact.class);
    @Setter
    private ChangeHandler changeHandler;

    public interface ChangeHandler{
        void onChange();
    }*/

    public EngineerEditorView() {
        //Создаем объекты для формы
        mainForm = new FormLayout();
        buttonLayout = new FormLayout();
        name = new TextField("Имя");
        proffesion = new TextField("Профессия");
        saveItem = new Button("Сохранить");
        cancel = new Button("Отмена");

        saveItem.getElement().getThemeList().add("primary");
        //Добавим все элементы на форму
        buttonLayout.add(saveItem, cancel);
        mainForm.add(name, proffesion);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(mainForm);
        verticalLayout.add(buttonLayout);
        setContent(verticalLayout);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer contactId) {
        id = contactId;
        if (!id.equals(0)) {
            addToNavbar(new H3("Редактирование записи"));
        } else {
            addToNavbar(new H3("Создание записи"));
        }
        fillForm();
    }


    private void fillForm() {

        if (!id.equals(0)) {
            Optional<Engineer> engineer = engineerRepository.findById(id);
            engineer.ifPresent(x -> {
                name.setValue(x.getName());
                proffesion.setValue(x.getProfession());
            });
        }

        saveItem.addClickListener(clickEvent -> {
            save();
            notificationSave();
        });

        cancel.addClickListener(clickEvent -> {
            notificationCancel();
        });
    }

    private void save(){
        Engineer engineer = new Engineer();
        if (!id.equals(0)) {
            engineer.setId(id);
        }
        engineer.setName(name.getValue());
        engineer.setProfession(proffesion.getValue());
        engineerRepository.save(engineer);
    }
    private void notificationSave(){
        Notification notification = new Notification(id.equals(0) ? "Запись успешно создана" : "Запись изменена", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(EngineerListView.class);
        });
        mainForm.setEnabled(false);
        notification.open();
    }
    private void notificationCancel(){
        Notification notification = new Notification("Отмена изменений", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(ContactListView.class);
        });
        mainForm.setEnabled(false);
        notification.open();
    }
}
