package ru.alex_brz.test.characteristic;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Route("characteristicEditor")
public class CharacteristicEditorView extends AppLayout implements HasUrlParameter<Integer> {

    Integer id;
    FormLayout contactForm;
    FormLayout buttonLayout;
    private TextField category;
    private TextField characteristicText;
    Button saveContact;
    Button cancel;

    @Autowired
    CharacteristicRepository characteristicRepository;

    public CharacteristicEditorView() {
        //Создаем объекты для формы
        contactForm = new FormLayout();
        buttonLayout = new FormLayout();
        category = new TextField(Characteristic.getCategories().get(1));
        characteristicText = new TextField(Characteristic.getCategories().get(2));
        saveContact = new Button("Сохранить");
        cancel = new Button("Отмена");

        saveContact.getElement().getThemeList().add("primary");
        //Добавим все элементы на форму
        buttonLayout.add(saveContact, cancel);
        contactForm.add(category,characteristicText);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(contactForm);
        verticalLayout.add(buttonLayout);
        setContent(verticalLayout);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer contactId) {
        id = contactId;
        if (!id.equals(0)) {
            addToNavbar(new H3("Редактировать характеристику"));
        } else {
            addToNavbar(new H3("Добавить характеристику"));
        }
        fillForm();
    }


    private void fillForm() {

        if (!id.equals(0)) {
            Optional<Characteristic> contact = characteristicRepository.findById(id);
            contact.ifPresent(x -> {
                category.setValue(x.getCategory());
                characteristicText.setValue(x.getCharacteristic());
            });
        }

        saveContact.addClickListener(clickEvent -> {
            save();
            notificationSave();
        });

        cancel.addClickListener(clickEvent -> {
            notificationCancel();
        });
    }

    private void save(){
        Characteristic characteristic = new Characteristic();
        if (!id.equals(0)) {
            characteristic.setId(id);
        }
        characteristic.setCategory(category.getValue());
        characteristic.setCharacteristic(characteristicText.getValue());
        characteristicRepository.save(characteristic);
    }
    private void notificationSave(){
        Notification notification = new Notification(id.equals(0) ? "Характеристика успешно создана" : "Харкатеристика была изменена", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(CharacteristicListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationCancel(){
        Notification notification = new Notification("Отмена изменений", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(CharacteristicListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
}
