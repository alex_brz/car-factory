package ru.alex_brz.test.used_areas;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.alex_brz.test.area.Area;
import ru.alex_brz.test.area.AreaRepository;
import ru.alex_brz.test.product.Product;
import ru.alex_brz.test.product.ProductRepository;

import java.util.Optional;

@Route("used_areas_Editor")
public class UsedAreaEditorView extends AppLayout implements HasUrlParameter<Integer> {

    Integer id;
    FormLayout contactForm;
    FormLayout buttonLayout;
    private TextField idArea;
    private TextField idProduct;
    Button saveContact;
    Button cancel;

    @Autowired
    UsedAreaRepository usedAreaRepository;
    @Autowired
    AreaRepository areaRepository;
    @Autowired
    ProductRepository productRepository;

    public UsedAreaEditorView() {
        //Создаем объекты для формы
        contactForm = new FormLayout();
        buttonLayout = new FormLayout();
        idProduct = new TextField(UsedArea.getCategories().get(1));
        idArea = new TextField(UsedArea.getCategories().get(2));
        saveContact = new Button("Сохранить");
        cancel = new Button("Отмена");

        saveContact.getElement().getThemeList().add("primary");
        //Добавим все элементы на форму
        buttonLayout.add(saveContact, cancel);
        contactForm.add(idProduct,idArea);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(contactForm);
        verticalLayout.add(buttonLayout);
        setContent(verticalLayout);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer contactId) {
        id = contactId;
        if (!id.equals(0)) {
            addToNavbar(new H3("Редактировать запись"));
        } else {
            addToNavbar(new H3("Добавить запись"));
        }
        fillForm();
    }


    private void fillForm() {

        if (!id.equals(0)) {
            Optional<UsedArea> contact = usedAreaRepository.findById(id);
            contact.ifPresent(x -> {
                idArea.setValue(String.valueOf(x.getIdArea()));
                idProduct.setValue(String.valueOf(x.getIdProduct()));
            });
        }

        saveContact.addClickListener(clickEvent -> {
            save();
            notificationSave();
        });

        cancel.addClickListener(clickEvent -> {
            notificationCancel();
        });
    }

    private void save(){
        UsedArea usedArea = new UsedArea();
        if (!id.equals(0)) {
            usedArea.setId(id);
        }
        Area area = areaRepository.findById(Integer.parseInt(idArea.getValue())).orElse(null);
        Product product = productRepository.findById(Integer.parseInt(idProduct.getValue())).orElse(null);
        if(area!=null&&product!=null) {
            usedArea.setIdArea(area);
            usedArea.setIdProduct(product);
        }else{
            notificationError();
        }
        usedAreaRepository.save(usedArea);
    }
    private void notificationSave(){
        Notification notification = new Notification(id.equals(0) ? "Запись успешно создана" : "Запись была изменена", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(UsedAreaListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationCancel(){
        Notification notification = new Notification("Отмена изменений", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(UsedAreaListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationError(){
        Notification notification = new Notification("Ошибка. Несуществующий id", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.open();
    }
}
