package ru.alex_brz.test.queries;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.alex_brz.test.area.AreaRepository;
import ru.alex_brz.test.guild.GuildRepository;
import ru.alex_brz.test.product.Product;
import ru.alex_brz.test.product.ProductRepository;

import javax.annotation.PostConstruct;
import java.util.List;

@Route("product_queries")
public class ProductQuery extends AppLayout {

    TextField categoryText = new TextField("Категория");
    TextField guildText = new TextField("Цех");
    TextField areaText = new TextField("Участок");
    TextField factoryText = new TextField("Предприятие");
    TimePicker startPicker = new TimePicker("Время от");
    TimePicker endPicker = new TimePicker("Время до");

    @Autowired
    ProductRepository productRepository;
    @Autowired
    GuildRepository guildRepository;
    @Autowired
    AreaRepository areaRepository;

    Grid<Product> grid = new Grid<>();
    public ProductQuery() {
        VerticalLayout layout = new VerticalLayout();



    }

    @PostConstruct
    public void fillGrid(){
        categoryText.setValueChangeMode(ValueChangeMode.EAGER);
        categoryText.addValueChangeListener(field -> filterList());

        guildText.setValueChangeMode(ValueChangeMode.EAGER);
        guildText.addValueChangeListener(field -> filterList());

        areaText.setValueChangeMode(ValueChangeMode.EAGER);
        areaText.addValueChangeListener(field -> filterList());

        factoryText.setValueChangeMode(ValueChangeMode.EAGER);
        factoryText.addValueChangeListener(field -> filterList());

        factoryText.setValueChangeMode(ValueChangeMode.EAGER);
        factoryText.addValueChangeListener(field -> filterList());

        startPicker.addValueChangeListener(field -> filterList());
        endPicker.addValueChangeListener(field -> filterList());

        List<Product> contacts = productRepository.findAll();
        if (!contacts.isEmpty()) {
            grid.addColumn(Product::getId).setHeader(Product.getCategories().get(0));
            grid.addColumn(Product::getName).setHeader(Product.getCategories().get(1));
            grid.addColumn(Product::getCategory).setHeader(Product.getCategories().get(2));
            grid.addColumn(Product::getStartTime).setHeader(Product.getCategories().get(3));
            grid.addColumn(Product::getEndTime).setHeader(Product.getCategories().get(4));
        }
    }

    private void filterList() {
        //productRepository.
    }
}
