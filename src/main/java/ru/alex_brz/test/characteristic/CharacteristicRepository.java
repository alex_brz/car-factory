package ru.alex_brz.test.characteristic;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CharacteristicRepository extends CrudRepository<Characteristic, Integer> {
    List<Characteristic> findAll();

}