package ru.alex_brz.test.test;

import lombok.Data;
import ru.alex_brz.test.laboratory.Laboratory;
import ru.alex_brz.test.test_engineer.TestEngineer;

import javax.persistence.*;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity(name = "Test")
@Data
//@EqualsAndHashCode
public class Test {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE}) ///*, targetEntity = Laboratory.class, optional = false*/)
    @JoinColumn(name = "lab_id")        //,insertable = false, updatable = false)
    private Laboratory idLaboratory;
    private Time time;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "idTest", orphanRemoval = true, cascade = {CascadeType.REMOVE})
    private List<TestEngineer> testEngineerList = new ArrayList<>();
    public void addTestEngineer(TestEngineer test){
        testEngineerList.add(test);
    }

    /*@OneToMany(fetch = FetchType.EAGER, mappedBy = "idTest", orphanRemoval = true, cascade = {CascadeType.REMOVE})
    private List<Equipment> equipmentList = new ArrayList<>();
    public void addEquipment(Equipment test){
        equipmentList.add(test);
    }*/

    private static List<String> categories = Arrays.asList("ID", "ID Лаборатории","Время");

    static List<String> getCategories() {
        return categories;
    }

    public Test() {
    }

    public Test(Time time) {
        this.time = time;
    }

    public Test(Laboratory idLaboratory, Time time) {
        this.idLaboratory = idLaboratory;
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Test test = (Test) o;
        return id == test.id &&
                Objects.equals(time, test.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, time);
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
