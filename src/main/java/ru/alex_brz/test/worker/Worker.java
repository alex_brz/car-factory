package ru.alex_brz.test.worker;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.alex_brz.test.brigade.Brigade;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity(name = "Worker")
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Worker {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Brigade idBrigade;
    private String profession;

    private static List<String> categories = Arrays.asList("ID", "ID Бригады","Профессия");

    static List<String> getCategories() {
        return categories;
    }

    public Worker(Brigade idBrigade, String profession) {
        this.idBrigade = idBrigade;
        this.profession = profession;
    }
    public Worker(String profession) {
        this.idBrigade = idBrigade;
        this.profession = profession;
    }
    @Override
    public String toString() {
        return id.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Worker worker = (Worker) o;
        return Objects.equals(id, worker.id) &&
                Objects.equals(profession, worker.profession);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, profession);
    }
}
