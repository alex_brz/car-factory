package ru.alex_brz.test.characteristic;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Arrays;
import java.util.List;

@Entity(name = "Characteristics")
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Characteristic {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String category;
    private String characteristic;

    private static List<String> categories = Arrays.asList("ID", "Категория товара","Хар-ка товара");

    static List<String> getCategories() {
        return categories;
    }

    public Characteristic(String category, String characteristic) {
        this.category = category;
        this.characteristic = characteristic;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
