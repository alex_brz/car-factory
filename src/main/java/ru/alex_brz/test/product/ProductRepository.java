package ru.alex_brz.test.product;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Integer> {
    List<Product> findAll();
    //List<Product> findDistinctByCategoriesStartsWith();
}