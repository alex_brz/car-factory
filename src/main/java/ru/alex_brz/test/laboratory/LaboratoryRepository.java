package ru.alex_brz.test.laboratory;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LaboratoryRepository extends CrudRepository<Laboratory, Integer> {
    List<Laboratory> findAll();
    //Laboratory findLaboratoryById(int id);
}