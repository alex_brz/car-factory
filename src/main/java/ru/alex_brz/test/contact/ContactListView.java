package ru.alex_brz.test.contact;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.NativeButtonRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;

@Route("contacts")
public class ContactListView extends AppLayout {

    VerticalLayout layout;
    Grid<Contact> grid;
    RouterLink linkCreate;
    TextField filter = new TextField();
    Button buttonDeleteTable = new Button("Удалить таблицу");
    ComboBox<String> categoryComboBox = new ComboBox<>();

    @Autowired
    ContactRepository contactRepository;

    public ContactListView() {
        layout = new VerticalLayout();
        grid = new Grid<>();
        linkCreate = new RouterLink("Создать контакт", ContactEditorView.class, 0);
        layout.add(linkCreate);

        HorizontalLayout filterLayout = new HorizontalLayout();
        filterLayout.add(categoryComboBox);
        filterLayout.add(filter);

        layout.add(filterLayout);
        layout.add(grid);
        addToNavbar(new H3("Список контактов"));
        addToNavbar(buttonDeleteTable);
        setContent(layout);
    }

    @PostConstruct
    public void fillGrid() {
        buttonDeleteTable.addClickListener(buttonClickEvent -> {
            Dialog dialog = new Dialog();
            Button confirm = new Button("Удалить", VaadinIcon.TRASH.create());
            confirm.getElement().getThemeList().add("error");
            Button cancel = new Button("Отмена", VaadinIcon.CLOSE_CIRCLE.create());
            dialog.add("Вы уверены что хотите удалить таблицу?");
            dialog.add(confirm);
            dialog.add(cancel);

            confirm.addClickListener(clickEvent -> {
                contactRepository.deleteAll();
                dialog.close();
                Notification notification = new Notification("Таблица очищена", 1000);
                notification.setPosition(Notification.Position.MIDDLE);
                notification.open();

                grid.setItems(contactRepository.findAll());

            });

            cancel.addClickListener(clickEvent -> {
                dialog.close();
            });

            dialog.open();

        });

        categoryComboBox.setPlaceholder("Choose category");
        categoryComboBox.setItems(Contact.getCategories());
        categoryComboBox.addValueChangeListener(category -> filterList(filter.getValue()));

        filter.setPlaceholder("Type to filter");
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(field -> filterList(field.getValue()));

        List<Contact> contacts = contactRepository.findAll();
        if (!contacts.isEmpty()) {

            //Выведем столбцы в нужном порядке
            grid.addColumn(Contact::getId).setHeader("ID");
            grid.addColumn(Contact::getFirstName).setHeader("Имя");
            grid.addColumn(Contact::getSecondName).setHeader("Фамилия");
            grid.addColumn(Contact::getFatherName).setHeader("Отчество");
            grid.addColumn(Contact::getNumberPhone).setHeader("Номер");
            grid.addColumn(Contact::getEmail).setHeader("E-mail");
            //Добавим кнопку удаления и редактирования
            grid.addColumn(new NativeButtonRenderer<>("Редактировать", contact -> {
                UI.getCurrent().navigate(ContactEditorView.class, contact.getId());
            }));
            grid.addColumn(new NativeButtonRenderer<>("Удалить", contact -> {
                Dialog dialog = new Dialog();
                Button confirm = new Button("Удалить", VaadinIcon.TRASH.create());
                confirm.getElement().getThemeList().add("error");
                Button cancel = new Button("Отмена", VaadinIcon.CLOSE_CIRCLE.create());
                dialog.add("Вы уверены что хотите удалить контакт?");
                dialog.add(confirm);
                dialog.add(cancel);

                confirm.addClickListener(clickEvent -> {
                    contactRepository.delete(contact);
                    dialog.close();
                    Notification notification = new Notification("Контакт удален", 1000);
                    notification.setPosition(Notification.Position.MIDDLE);
                    notification.open();

                    grid.setItems(contactRepository.findAll());

                });

                cancel.addClickListener(clickEvent -> {
                    dialog.close();
                });

                dialog.open();

            }));

            for (Grid.Column column : grid.getColumns()) {
                column.setSortable(true);
            }
            grid.setItems(contacts);

        }
    }

    private void filterList(String value){
        int i = Contact.getCategories().indexOf(categoryComboBox.getValue());
        switch (i){
            case 0:
                grid.setItems(contactRepository.findContactsById(Integer.parseInt(value)));
                break;
            case 1: grid.setItems(contactRepository.findContactsByFirstNameIsContaining(value));
                break;
            case 2: grid.setItems(contactRepository.findContactsBySecondNameIsContaining(value));
                break;
            case 3: grid.setItems(contactRepository.findContactsByFatherNameIsContaining(value));
                break;
            case 4: grid.setItems(contactRepository.findContactsByNumberPhoneIsContaining(value));
                break;
            case 5: grid.setItems(contactRepository.findContactsByEmailIsContaining(value));
                break;
        }
    }


}
