package ru.alex_brz.test.product;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Time;
import java.util.Arrays;
import java.util.List;

@Entity(name = "Products")
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String category;
    private Time startTime;
    private Time endTime;

    private static List<String> categories = Arrays.asList("ID", "Название","Категория", "Время начала","Время окончания");

    public static List<String> getCategories() {
        return categories;
    }

    public Product(String name, String category, Time startTime, Time endTime) {
        this.name = name;
        this.category = category;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
