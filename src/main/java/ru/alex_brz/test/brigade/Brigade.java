package ru.alex_brz.test.brigade;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.alex_brz.test.area.Area;
import ru.alex_brz.test.product.Product;
import ru.alex_brz.test.worker.Worker;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity(name = "Brigades")
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Brigade {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Area idArea;
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Worker idBrigadier;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Product idProduct;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "idBrigade",orphanRemoval = true, cascade = {CascadeType.REMOVE})
    private List<Worker> workerList = new ArrayList<>();
    public void addWorker(Worker test){
        workerList.add(test);
    }

    private static List<String> categories = Arrays.asList("ID", "ID Участка","ID Бригадира","ID Товара");

    static List<String> getCategories() {
        return categories;
    }

    public Brigade(Area idArea, Worker idBrigadier, Product idProduct) {
        this.idArea = idArea;
        this.idBrigadier = idBrigadier;
        this.idProduct = idProduct;
    }
    @Override
    public String toString() {
        return id.toString();
    }
}
