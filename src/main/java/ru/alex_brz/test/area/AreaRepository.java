package ru.alex_brz.test.area;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AreaRepository extends CrudRepository<Area, Integer> {
    List<Area> findAll();
    Area findByIdHead_Id(Integer id);
    List<Area> findAreasById(Integer id);
    List<Area> findAreasByIdGuild_Id(Integer id);
    List<Area> findAreasByIdHead_Id(Integer id);
    List<Area> findAreasByIdProduct_Id(Integer id);



}