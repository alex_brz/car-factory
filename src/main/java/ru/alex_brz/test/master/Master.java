package ru.alex_brz.test.master;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.alex_brz.test.engineer.Engineer;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;

@Entity(name = "Masters")
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Master {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Engineer id_engineer;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Engineer id_area_head;

    private static List<String> categories = Arrays.asList("ID", "ID Инженера","ID Начальника участка");

    static List<String> getCategories() {
        return categories;
    }

    public Master(Engineer id_engineer, Engineer id_area_head) {
        this.id_engineer = id_engineer;
        this.id_area_head = id_area_head;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
