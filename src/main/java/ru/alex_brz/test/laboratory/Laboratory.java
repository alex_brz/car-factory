package ru.alex_brz.test.laboratory;

import lombok.Data;
import ru.alex_brz.test.test.Test;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity(name = "Laboratory")
@Data
public class Laboratory {
    @Id
    //@Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "idLaboratory",orphanRemoval = true, cascade = {CascadeType.REMOVE})
    private List<Test> testList = new ArrayList<>();

    public void addTest(Test test){
        testList.add(test);
    }

    @Transient
    private static List<String> categories = Arrays.asList("ID");

    static List<String> getCategories() {
        return categories;
    }


    public Laboratory() {
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Laboratory that = (Laboratory) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
