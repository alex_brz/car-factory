package ru.alex_brz.test.equipment;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.alex_brz.test.test.Test;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;

@Entity(name = "Equipment")
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Equipment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Test idTest;
    private String name;

    private static List<String> categories = Arrays.asList("ID", "ID Испытания","Название");

    static List<String> getCategories() {
        return categories;
    }

    public Equipment(Test idTest, String name) {
        this.idTest = idTest;
        this.name = name;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
