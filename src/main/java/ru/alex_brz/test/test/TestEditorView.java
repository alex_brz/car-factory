package ru.alex_brz.test.test;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.alex_brz.test.laboratory.Laboratory;
import ru.alex_brz.test.laboratory.LaboratoryRepository;

import java.sql.Time;
import java.time.LocalTime;
import java.util.Optional;

@Route("testEditor")
public class TestEditorView extends AppLayout implements HasUrlParameter<Integer> {

    private Integer id;
    FormLayout contactForm;
    FormLayout buttonLayout;
    private TextField idLabText;
    private TimePicker timeText;
    Button saveContact;
    Button cancel;

    @Autowired
    TestRepository testRepository;
    @Autowired
    LaboratoryRepository laboRepository;

    public TestEditorView() {
        //Создаем объекты для формы
        contactForm = new FormLayout();
        buttonLayout = new FormLayout();
        idLabText = new TextField(Test.getCategories().get(1));
        timeText = new TimePicker(Test.getCategories().get(2));
        saveContact = new Button("Сохранить");
        cancel = new Button("Отмена");

        saveContact.getElement().getThemeList().add("primary");
        //Добавим все элементы на форму
        buttonLayout.add(saveContact, cancel);
        contactForm.add(idLabText, timeText);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(contactForm);
        verticalLayout.add(buttonLayout);
        setContent(verticalLayout);
    }
    @Transactional
    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer contactId) {
        id = contactId;
        if (!id.equals(0)) {
            addToNavbar(new H3("Редактировать испытание"));
        } else {
            addToNavbar(new H3("Добавить испытание"));
        }
        fillForm();
    }


    private void fillForm() {

        if (!id.equals(0)) {
            Optional<Test> equipment = testRepository.findById(id);
            equipment.ifPresent(x -> {
                idLabText.setValue(String.valueOf(x.getIdLaboratory()));
                timeText.setValue(LocalTime.parse(x.getTime().toString()));
            });
        }

        saveContact.addClickListener(clickEvent -> {
            save();
            notificationSave();
        });

        cancel.addClickListener(clickEvent -> {
            notificationCancel();
        });
    }

    private void save(){
        Test test = new Test();
        if (!id.equals(0)) {
            test.setId(id);
        }

        Laboratory laboratory = laboRepository.findById(Integer.parseInt(idLabText.getValue())).orElse(null); //нашел лабораторию по введенному id
        if(laboratory!=null){ //если она существует, то
            laboratory.addTest(test); //добавляем в ее list новый test
            test.setIdLaboratory(laboratory); //обавляем в test лабораторию
            test.setTime(Time.valueOf(timeText.getValue())); //указываем еще один праметр

            laboRepository.save(laboratory); //сохраняем лабораторию
            /*transactional е*/
            testRepository.save(test);//сохраняем тест
        }else{
            notificationError();
        }
    }
    private void notificationSave(){
        Notification notification = new Notification(id.equals(0) ?
                "Испытание успешно создано" : "Испытание было изменено", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(TestListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationCancel(){
        Notification notification = new Notification("Отмена изменений", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(TestListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationError(){
        Notification notification = new Notification("Ошибка. Несуществующий id", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.open();
    }
}
