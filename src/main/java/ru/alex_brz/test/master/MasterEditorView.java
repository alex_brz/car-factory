package ru.alex_brz.test.master;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.alex_brz.test.area.Area;
import ru.alex_brz.test.area.AreaRepository;
import ru.alex_brz.test.contact.ContactListView;
import ru.alex_brz.test.engineer.Engineer;
import ru.alex_brz.test.engineer.EngineerRepository;

import java.util.Optional;

@Route("masterEditor")
public class MasterEditorView extends AppLayout implements HasUrlParameter<Integer> {

    Integer id;
    FormLayout mainForm;
    FormLayout buttonLayout;
    TextField idEngineer;
    TextField idAreaHead;
    Button saveItem;
    Button cancel;

    @Autowired
    MasterRepository masterRepository;
    @Autowired
    EngineerRepository engineerRepository;
    @Autowired
    AreaRepository areaRepository;

    /*Binder<Contact> binder = new Binder<>(Contact.class);
    @Setter
    private ChangeHandler changeHandler;

    public interface ChangeHandler{
        void onChange();
    }*/

    public MasterEditorView() {
        //Создаем объекты для формы
        mainForm = new FormLayout();
        buttonLayout = new FormLayout();
        idEngineer = new TextField("ID Инженера");
        idAreaHead = new TextField("ID Начальника участка");
        saveItem = new Button("Сохранить");
        cancel = new Button("Отмена");

        saveItem.getElement().getThemeList().add("primary");
        //Добавим все элементы на форму
        buttonLayout.add(saveItem, cancel);
        mainForm.add(idEngineer, idAreaHead);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(mainForm);
        verticalLayout.add(buttonLayout);
        setContent(verticalLayout);
    }
    @Transactional
    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer contactId) {
        id = contactId;
        if (!id.equals(0)) {
            addToNavbar(new H3("Редактирование записи"));
        } else {
            addToNavbar(new H3("Создание записи"));
        }
        fillForm();
    }


    private void fillForm() {

        if (!id.equals(0)) {
            Optional<Master> engineer = masterRepository.findById(id);
            engineer.ifPresent(x -> {
                idEngineer.setValue(String.valueOf(x.getId_engineer()));
                idAreaHead.setValue(String.valueOf(x.getId_area_head()));
            });
        }

        saveItem.addClickListener(clickEvent -> {
            save();
            notificationSave();
        });

        cancel.addClickListener(clickEvent -> {
            notificationCancel();
        });
    }

    private void save(){
        Master master = new Master();
        if (!id.equals(0)) {
            master.setId(id);
        }
        Engineer areaHead = engineerRepository.findById(Integer.parseInt(idAreaHead.getValue())).orElse(null);
        Engineer engineer = engineerRepository.findById(Integer.parseInt(idEngineer.getValue())).orElse(null);
        Area area = areaRepository.findByIdHead_Id(Integer.parseInt(idAreaHead.getValue()));
        if(areaHead!=null&&engineer!=null&&area!=null) {

            master.setId_engineer(engineer);
            master.setId_area_head(areaHead);

            engineer.addMaster(master);
            area.addMaster(master);

            engineerRepository.save(engineer);
            areaRepository.save(area);
            masterRepository.save(master);
        }else{
            notificationError();
        }
    }
    private void notificationSave(){
        Notification notification = new Notification(id.equals(0) ? "Запись успешно создана" : "Запись изменена", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(MasterListView.class);
        });
        mainForm.setEnabled(false);
        notification.open();
    }
    private void notificationCancel(){
        Notification notification = new Notification("Отмена изменений", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(ContactListView.class);
        });
        mainForm.setEnabled(false);
        notification.open();
    }
    private void notificationError(){
        Notification notification = new Notification("Ошибка. Несуществующий id", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.open();
    }
}
