package ru.alex_brz.test.area;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.alex_brz.test.engineer.Engineer;
import ru.alex_brz.test.engineer.EngineerRepository;
import ru.alex_brz.test.guild.Guild;
import ru.alex_brz.test.guild.GuildRepository;
import ru.alex_brz.test.product.Product;
import ru.alex_brz.test.product.ProductRepository;

import java.util.Optional;

@Route("areaEditor")
public class AreaEditorView extends AppLayout implements HasUrlParameter<Integer> {

    Integer id;
    FormLayout contactForm;
    FormLayout buttonLayout;
    private TextField idGuild;
    private TextField idHead;
    private TextField idProduct;
    Button saveContact;
    Button cancel;

    @Autowired
    AreaRepository areaRepository;
    @Autowired
    EngineerRepository engineerRepository;
    @Autowired
    GuildRepository guildRepository;
    @Autowired
    ProductRepository productRepository;

    public AreaEditorView() {
        //Создаем объекты для формы
        contactForm = new FormLayout();
        buttonLayout = new FormLayout();
        idGuild = new TextField(Area.getCategories().get(1));
        idHead = new TextField(Area.getCategories().get(2));
        idProduct = new TextField(Area.getCategories().get(3));
        saveContact = new Button("Сохранить");
        cancel = new Button("Отмена");

        saveContact.getElement().getThemeList().add("primary");
        //Добавим все элементы на форму
        buttonLayout.add(saveContact, cancel);
        contactForm.add(idGuild,idHead, idProduct);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(contactForm);
        verticalLayout.add(buttonLayout);
        setContent(verticalLayout);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer contactId) {
        id = contactId;
        if (!id.equals(0)) {
            addToNavbar(new H3("Редактировать участок"));
        } else {
            addToNavbar(new H3("Добавить участок"));
        }
        fillForm();
    }


    private void fillForm() {

        if (!id.equals(0)) {
            Optional<Area> contact = areaRepository.findById(id);
            contact.ifPresent(x -> {
                idGuild.setValue(String.valueOf(x.getIdGuild()));
                idHead.setValue(String.valueOf(x.getIdHead()));
                idProduct.setValue(String.valueOf(x.getIdProduct()));
            });
        }

        saveContact.addClickListener(clickEvent -> {
            save();
            notificationSave();
        });

        cancel.addClickListener(clickEvent -> {
            notificationCancel();
        });
    }

    private void save(){
        Area area = new Area();
        if (!id.equals(0)) {
            area.setId(id);
        }
        Guild guild = guildRepository.findById(Integer.parseInt(idGuild.getValue())).orElse(null);
        Engineer engineer = engineerRepository.findById(Integer.parseInt(idHead.getValue())).orElse(null);
        Product product = productRepository.findById(Integer.parseInt(idProduct.getValue())).orElse(null);
        if(guild!=null&&engineer!=null&&product!=null) {
            area.setIdGuild(guild);
            area.setIdHead(engineer);
            area.setIdProduct(product);
        }else{
            notificationError();
        }
        areaRepository.save(area);
    }
    private void notificationSave(){
        Notification notification = new Notification(id.equals(0) ? "Участок успешно создан" : "Участок был изменен", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(AreaListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationCancel(){
        Notification notification = new Notification("Отмена изменений", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(AreaListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationError(){
        Notification notification = new Notification("Ошибка. Несуществующий id", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.open();
    }
}
