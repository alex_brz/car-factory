package ru.alex_brz.test.product;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Time;
import java.time.LocalTime;
import java.util.Optional;

@Route("productEditor")
public class ProductEditorView extends AppLayout implements HasUrlParameter<Integer> {

    private Integer id;
    FormLayout contactForm;
    FormLayout buttonLayout;
    private TextField nameText;
    private TextField categoryText;
    private TimePicker startText;
    private TimePicker endText;
    Button saveContact;
    Button cancel;

    @Autowired
    ProductRepository productRepository;

    public ProductEditorView() {
        //Создаем объекты для формы
        contactForm = new FormLayout();
        buttonLayout = new FormLayout();
        nameText = new TextField(Product.getCategories().get(1));
        categoryText = new TextField(Product.getCategories().get(2));
        //startText = new TextField(Product.getCategories().get(3));
        startText = new TimePicker(Product.getCategories().get(3));
        endText = new TimePicker(Product.getCategories().get(4));
        saveContact = new Button("Сохранить");
        cancel = new Button("Отмена");

        saveContact.getElement().getThemeList().add("primary");
        //Добавим все элементы на форму
        buttonLayout.add(saveContact, cancel);
        contactForm.add(nameText, categoryText, startText, endText);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(contactForm);
        verticalLayout.add(buttonLayout);
        setContent(verticalLayout);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer contactId) {
        id = contactId;
        if (!id.equals(0)) {
            addToNavbar(new H3("Редактировать товар"));
        } else {
            addToNavbar(new H3("Добавить товар"));
        }
        fillForm();
    }


    private void fillForm() {

        if (!id.equals(0)) {
            Optional<Product> equipment = productRepository.findById(id);
            equipment.ifPresent(x -> {
                nameText.setValue(x.getName());
                categoryText.setValue(x.getCategory());
                startText.setValue(LocalTime.parse(x.getStartTime().toString()));
                endText.setValue(LocalTime.parse(x.getEndTime().toString()));
            });
        }

        saveContact.addClickListener(clickEvent -> {
            save();
            notificationSave();
        });

        cancel.addClickListener(clickEvent -> {
            notificationCancel();
        });
    }

    private void save(){
        Product product = new Product();
        if (!id.equals(0)) {
            product.setId(id);
        }
        product.setName(nameText.getValue());
        product.setCategory(categoryText.getValue());
        product.setStartTime(Time.valueOf(startText.getValue()));
        product.setEndTime(Time.valueOf(endText.getValue()));
        productRepository.save(product);
    }
    private void notificationSave(){
        Notification notification = new Notification(id.equals(0) ?
                "Товар успешно создан" : "Товар был изменен", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(ProductListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationCancel(){
        Notification notification = new Notification("Отмена изменений", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(ProductListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
}
