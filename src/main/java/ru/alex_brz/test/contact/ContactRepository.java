package ru.alex_brz.test.contact;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContactRepository extends CrudRepository<Contact, Integer> {
    List<Contact> findAll();
    List<Contact> findContactsById(int i);
    List<Contact> findContactsByFirstNameIsContaining(String value);
    List<Contact> findContactsBySecondNameIsContaining(String value);
    List<Contact> findContactsByFatherNameIsContaining(String value);
    List<Contact> findContactsByEmailIsContaining(String value);
    List<Contact> findContactsByNumberPhoneIsContaining(String value);

}