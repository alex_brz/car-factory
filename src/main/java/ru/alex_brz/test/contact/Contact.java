package ru.alex_brz.test.contact;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Arrays;
import java.util.List;

@Entity(name = "Contacts")
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String firstName;
    private String secondName;
    private String fatherName;
    private String numberPhone;
    private String email;

    private static List<String> categories = Arrays.asList("ID", "Имя","Фамилия","Отчество","Номер","E-mail");

    static List<String> getCategories() {
        return categories;
    }

    public Contact(String firstName, String secondName, String fatherName, String numberPhone, String email) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.fatherName = fatherName;
        this.numberPhone = numberPhone;
        this.email = email;
    }
}
