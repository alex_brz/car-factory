package ru.alex_brz.test.worker;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.alex_brz.test.brigade.Brigade;
import ru.alex_brz.test.brigade.BrigadeRepository;

import java.util.Optional;

@Route("workerEditor")
public class WorkerEditorView extends AppLayout implements HasUrlParameter<Integer> {

    private Integer id;
    FormLayout contactForm;
    FormLayout buttonLayout;
    private TextField brigadeText;
    private TextField profText;
    Button saveContact;
    Button cancel;

    @Autowired
    WorkerRepository workerRepository;
    @Autowired
    BrigadeRepository brigadeRepository;

    public WorkerEditorView() {
        //Создаем объекты для формы
        contactForm = new FormLayout();
        buttonLayout = new FormLayout();
        brigadeText = new TextField(Worker.getCategories().get(1));
        profText = new TextField(Worker.getCategories().get(2));
        saveContact = new Button("Сохранить");
        cancel = new Button("Отмена");

        saveContact.getElement().getThemeList().add("primary");
        //Добавим все элементы на форму
        buttonLayout.add(saveContact, cancel);
        contactForm.add(brigadeText, profText);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(contactForm);
        verticalLayout.add(buttonLayout);
        setContent(verticalLayout);
    }
    @Transactional
    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer contactId) {
        id = contactId;
        if (!id.equals(0)) {
            addToNavbar(new H3("Редактировать рабочего"));
        } else {
            addToNavbar(new H3("Добавить рабочего"));
        }
        fillForm();
    }


    private void fillForm() {

        if (!id.equals(0)) {
            Optional<Worker> equipment = workerRepository.findById(id);
            equipment.ifPresent(x -> {
                brigadeText.setValue(String.valueOf(x.getIdBrigade()));
                profText.setValue(x.getProfession());
            });
        }

        saveContact.addClickListener(clickEvent -> {
            save();
            notificationSave();
        });

        cancel.addClickListener(clickEvent -> {
            notificationCancel();
        });
    }

    private void save(){
        Worker worker = new Worker();
        if (!id.equals(0)) {
            worker.setId(id);
        }
        Brigade brigade = brigadeRepository.findById(Integer.parseInt(brigadeText.getValue())).orElse(null);
        if(brigade!=null) {
            worker.setIdBrigade(brigade);
            worker.setProfession(profText.getValue());

            brigade.addWorker(worker);
            brigadeRepository.save(brigade);
            workerRepository.save(worker);
        }else{
            notificationError();
        }
    }
    private void notificationSave(){
        Notification notification = new Notification(id.equals(0) ?
                "Рабочий успешно создан" : "Рабочий был изменен", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(WorkerListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationCancel(){
        Notification notification = new Notification("Отмена изменений", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(WorkerListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationError(){
        Notification notification = new Notification("Ошибка. Несуществующий id", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.open();
    }
}
