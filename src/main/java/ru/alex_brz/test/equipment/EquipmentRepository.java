package ru.alex_brz.test.equipment;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EquipmentRepository extends CrudRepository<Equipment, Integer> {
    List<Equipment> findAll();

}