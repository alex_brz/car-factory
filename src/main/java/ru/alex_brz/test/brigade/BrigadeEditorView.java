package ru.alex_brz.test.brigade;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.alex_brz.test.area.Area;
import ru.alex_brz.test.area.AreaRepository;
import ru.alex_brz.test.product.Product;
import ru.alex_brz.test.product.ProductRepository;
import ru.alex_brz.test.worker.Worker;
import ru.alex_brz.test.worker.WorkerRepository;

import java.util.Optional;

@Route("brigadeEditor")
public class BrigadeEditorView extends AppLayout implements HasUrlParameter<Integer> {

    Integer id;
    FormLayout contactForm;
    FormLayout buttonLayout;
    private TextField idArea;
    private TextField idBrigadier;
    private TextField idProduct;
    Button saveContact;
    Button cancel;

    @Autowired
    BrigadeRepository brigadeRepository;
    @Autowired
    AreaRepository areaRepository;
    @Autowired
    WorkerRepository workerRepository;
    @Autowired
    ProductRepository productRepository;

    public BrigadeEditorView() {
        //Создаем объекты для формы
        contactForm = new FormLayout();
        buttonLayout = new FormLayout();
        idArea = new TextField(Brigade.getCategories().get(1));
        idBrigadier = new TextField(Brigade.getCategories().get(2));
        idProduct = new TextField(Brigade.getCategories().get(3));
        saveContact = new Button("Сохранить");
        cancel = new Button("Отмена");

        saveContact.getElement().getThemeList().add("primary");
        //Добавим все элементы на форму
        buttonLayout.add(saveContact, cancel);
        contactForm.add(idArea, idBrigadier, idProduct);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(contactForm);
        verticalLayout.add(buttonLayout);
        setContent(verticalLayout);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer contactId) {
        id = contactId;
        if (!id.equals(0)) {
            addToNavbar(new H3("Редактировать участок"));
        } else {
            addToNavbar(new H3("Добавить участок"));
        }
        fillForm();
    }


    private void fillForm() {

        if (!id.equals(0)) {
            Optional<Brigade> contact = brigadeRepository.findById(id);
            contact.ifPresent(x -> {
                idArea.setValue(String.valueOf(x.getIdArea()));
                idBrigadier.setValue(String.valueOf(x.getIdBrigadier()));
                idProduct.setValue(String.valueOf(x.getIdProduct()));
            });
        }

        saveContact.addClickListener(clickEvent -> {
            save();
            notificationSave();
        });

        cancel.addClickListener(clickEvent -> {
            notificationCancel();
        });
    }

    private void save(){
        Brigade brigade = new Brigade();
        if (!id.equals(0)) {
            brigade.setId(id);
        }

        Area area = areaRepository.findById(Integer.parseInt(idArea.getValue())).orElse(null);
        Worker worker = workerRepository.findById(Integer.parseInt(idBrigadier.getValue())).orElse(null);
        Product product = productRepository.findById(Integer.parseInt(idProduct.getValue())).orElse(null);
        if(area!=null&&worker!=null&&product!=null) {
            brigade.setIdArea(area);
            brigade.setIdBrigadier(worker);
            brigade.setIdProduct(product);

            brigadeRepository.save(brigade);
        }else{
            notificationError();
        }
    }
    private void notificationSave(){
        Notification notification = new Notification(id.equals(0) ? "Бригада успешно создана" : "Бригада была изменена", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(BrigadeListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationCancel(){
        Notification notification = new Notification("Отмена изменений", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(BrigadeListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationError(){
        Notification notification = new Notification("Ошибка. Несуществующий id", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.open();
    }
}
