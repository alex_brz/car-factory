package ru.alex_brz.test.engineer;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EngineerRepository extends CrudRepository<Engineer, Integer> {
    List<Engineer> findAll();
}