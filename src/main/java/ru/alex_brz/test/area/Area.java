package ru.alex_brz.test.area;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.alex_brz.test.engineer.Engineer;
import ru.alex_brz.test.guild.Guild;
import ru.alex_brz.test.master.Master;
import ru.alex_brz.test.product.Product;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity(name = "Areas")
@Data
@EqualsAndHashCode
public class Area {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Guild idGuild;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Engineer idHead;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Product idProduct;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "id_area_head",orphanRemoval = true, cascade = {CascadeType.REMOVE})
    private List<Master> masterList = new ArrayList<>();
    public void addMaster(Master test){
        masterList.add(test);
    }

    @org.springframework.data.annotation.Transient
    private static List<String> categories = Arrays.asList("ID", "ID Цеха","ID Начальника","ID Товара");

    static List<String> getCategories() {
        return categories;
    }

    public Area() {
    }

    public Area(Guild idGuild, Engineer idHead, Product idProduct) {
        this.idGuild = idGuild;
        this.idHead = idHead;
        this.idProduct = idProduct;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
