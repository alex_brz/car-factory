package ru.alex_brz.test.used_areas;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.alex_brz.test.area.Area;
import ru.alex_brz.test.product.Product;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;

@Entity(name = "used_areas")
@Data
@EqualsAndHashCode
public class UsedArea {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Product idProduct;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Area idArea;

    private static List<String> categories = Arrays.asList("ID", "ID Изделия","ID Участка");

    static List<String> getCategories() {
        return categories;
    }

    public UsedArea() {
    }

    public UsedArea(Product idProduct, Area idArea) {
        this.idProduct = idProduct;
        this.idArea = idArea;
    }
    @Override
    public String toString() {
        return id.toString();
    }

}
