package ru.alex_brz.test.used_tests;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.alex_brz.test.product.Product;
import ru.alex_brz.test.product.ProductRepository;
import ru.alex_brz.test.test.Test;
import ru.alex_brz.test.test.TestRepository;

import java.util.Optional;

@Route("used_tests_Editor")
public class UsedTestEditorView extends AppLayout implements HasUrlParameter<Integer> {

    Integer id;
    FormLayout contactForm;
    FormLayout buttonLayout;
    private TextField idProduct;
    private TextField idTest;
    Button saveContact;
    Button cancel;

    @Autowired
    UsedTestRepository usedTestRepository;
    @Autowired
    TestRepository testRepository;
    @Autowired
    ProductRepository productRepository;

    public UsedTestEditorView() {
        //Создаем объекты для формы
        contactForm = new FormLayout();
        buttonLayout = new FormLayout();
        idProduct = new TextField(UsedTest.getCategories().get(1));
        idTest = new TextField(UsedTest.getCategories().get(2));
        saveContact = new Button("Сохранить");
        cancel = new Button("Отмена");

        saveContact.getElement().getThemeList().add("primary");
        //Добавим все элементы на форму
        buttonLayout.add(saveContact, cancel);
        contactForm.add(idProduct, idTest);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(contactForm);
        verticalLayout.add(buttonLayout);
        setContent(verticalLayout);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer contactId) {
        id = contactId;
        if (!id.equals(0)) {
            addToNavbar(new H3("Редактировать запись"));
        } else {
            addToNavbar(new H3("Добавить запись"));
        }
        fillForm();
    }


    private void fillForm() {

        if (!id.equals(0)) {
            Optional<UsedTest> contact = usedTestRepository.findById(id);
            contact.ifPresent(x -> {
                idProduct.setValue(String.valueOf(x.getIdProduct()));
                idTest.setValue(String.valueOf(x.getIdTest()));
            });
        }

        saveContact.addClickListener(clickEvent -> {
            save();
            notificationSave();
        });

        cancel.addClickListener(clickEvent -> {
            notificationCancel();
        });
    }

    private void save(){
        UsedTest usedTest = new UsedTest();
        if (!id.equals(0)) {
            usedTest.setId(id);
        }
        Test test = testRepository.findById(Integer.parseInt(idTest.getValue())).orElse(null);
        Product product = productRepository.findById(Integer.parseInt(idProduct.getValue())).orElse(null);
        if(test!=null&&product!=null) {
            usedTest.setIdTest(test);
            usedTest.setIdProduct(product);
        }else{
            notificationError();
        }
        usedTestRepository.save(usedTest);
    }
    private void notificationSave(){
        Notification notification = new Notification(id.equals(0) ? "Запись успешно создана" : "Запись была изменена", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(UsedTestListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationCancel(){
        Notification notification = new Notification("Отмена изменений", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(UsedTestListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationError(){
        Notification notification = new Notification("Ошибка. Несуществующий id", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.open();
    }
}
