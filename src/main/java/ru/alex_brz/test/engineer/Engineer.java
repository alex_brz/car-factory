package ru.alex_brz.test.engineer;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.alex_brz.test.master.Master;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity(name = "Engineers")
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Engineer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String profession;

    /*@OneToMany(fetch = FetchType.EAGER, mappedBy = "idHead",orphanRemoval = true, cascade = {CascadeType.REMOVE})
    private List<Area> areaList = new ArrayList<>();
    public void addArea(Area test){
        areaList.add(test);
    }*/

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "id_engineer",orphanRemoval = true, cascade = {CascadeType.REMOVE})
    private List<Master> masterList = new ArrayList<>();
    public void addMaster(Master test){
        masterList.add(test);
    }

    private static List<String> categories = Arrays.asList("ID", "Имя","Профессия");

    static List<String> getCategories() {
        return categories;
    }

    public Engineer(String name, String profession) {
        this.name = name;
        this.profession = profession;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
