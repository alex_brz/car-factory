package ru.alex_brz.test.area;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.NativeButtonRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.beans.factory.annotation.Autowired;
import ru.alex_brz.test.MainView;

import javax.annotation.PostConstruct;
import java.util.List;

@Route("areas")
public class AreaListView extends AppLayout {

    VerticalLayout layout;
    Grid<Area> grid;
    RouterLink linkCreate;
    TextField filter = new TextField();
    Button buttonDeleteTable = new Button("Удалить таблицу");
    ComboBox<String> categoryComboBox = new ComboBox<>();

    @Autowired
    AreaRepository areaRepository;

    public AreaListView() {
        layout = new VerticalLayout();
        grid = new Grid<>();
        linkCreate = new RouterLink("Создать участок", AreaEditorView.class, 0);
        layout.add(linkCreate);

        HorizontalLayout filterLayout = new HorizontalLayout();
        filterLayout.add(categoryComboBox);
        filterLayout.add(filter);

        layout.add(filterLayout);
        layout.add(grid);
        addToNavbar(new H3("Список участков"));
        addToNavbar(buttonDeleteTable);
        MenuBar menuBar = new MenuBar();
        menuBar.addItem("Hello!");
        menuBar.addItem(new RouterLink("Main page", MainView.class));
        addToNavbar(menuBar);
        setContent(layout);
    }

    @PostConstruct
    public void fillGrid() {
        buttonDeleteTable.addClickListener(buttonClickEvent -> {
            Dialog dialog = new Dialog();
            Button confirm = new Button("Удалить", VaadinIcon.TRASH.create());
            confirm.getElement().getThemeList().add("error");
            Button cancel = new Button("Отмена", VaadinIcon.CLOSE_CIRCLE.create());
            dialog.add("Вы уверены что хотите удалить таблицу?");
            dialog.add(confirm);
            dialog.add(cancel);

            confirm.addClickListener(clickEvent -> {
                areaRepository.deleteAll();
                dialog.close();
                Notification notification = new Notification("Таблица очищена", 1000);
                notification.setPosition(Notification.Position.MIDDLE);
                notification.open();

                grid.setItems(areaRepository.findAll());

            });

            cancel.addClickListener(clickEvent -> {
                dialog.close();
            });

            dialog.open();

        });

        categoryComboBox.setPlaceholder("Choose category");
        categoryComboBox.setItems(Area.getCategories());
        categoryComboBox.addValueChangeListener(category -> filterList(filter.getValue()));

        filter.setPlaceholder("Type to filter");
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(field -> filterList(field.getValue()));

        List<Area> contacts = areaRepository.findAll();
        if (!contacts.isEmpty()) {

            //Выведем столбцы в нужном порядке
            grid.addColumn(Area::getId).setHeader(Area.getCategories().get(0));
            grid.addColumn(Area::getIdGuild).setHeader(Area.getCategories().get(1));
            grid.addColumn(Area::getIdHead).setHeader(Area.getCategories().get(2));
            grid.addColumn(Area::getIdProduct).setHeader(Area.getCategories().get(3));
            //Добавим кнопку удаления и редактирования
            grid.addColumn(new NativeButtonRenderer<>("Редактировать", contact -> {
                UI.getCurrent().navigate(AreaEditorView.class, contact.getId());
            }));
            grid.addColumn(new NativeButtonRenderer<>("Удалить", contact -> {
                Dialog dialog = new Dialog();
                Button confirm = new Button("Удалить", VaadinIcon.TRASH.create());
                confirm.getElement().getThemeList().add("error");
                Button cancel = new Button("Отмена", VaadinIcon.CLOSE_CIRCLE.create());
                dialog.add("Вы уверены что хотите удалить контакт?");
                dialog.add(confirm);
                dialog.add(cancel);

                confirm.addClickListener(clickEvent -> {
                    areaRepository.delete(contact);
                    dialog.close();
                    Notification notification = new Notification("Контакт удален", 1000);
                    notification.setPosition(Notification.Position.MIDDLE);
                    notification.open();

                    grid.setItems(areaRepository.findAll());

                });

                cancel.addClickListener(clickEvent -> {
                    dialog.close();
                });

                dialog.open();

            }));

            for (Grid.Column column : grid.getColumns()) {
                column.setSortable(true);
            }
            grid.setItems(contacts);

        }
    }

    private void filterList(String value){
        int i = Area.getCategories().indexOf(categoryComboBox.getValue());
        if(value.isEmpty()){
            grid.setItems(areaRepository.findAll());
            return;
        }
        switch (i){
            case 0:
                grid.setItems(areaRepository.findAreasById(Integer.parseInt(value)));
                break;
            case 1: grid.setItems(areaRepository.findAreasByIdGuild_Id(Integer.parseInt(value)));
                break;
            case 2: grid.setItems(areaRepository.findByIdHead_Id(Integer.parseInt(value)));
                break;
            case 3: grid.setItems(areaRepository.findAreasByIdProduct_Id(Integer.parseInt(value)));
                break;
        }
    }


}
