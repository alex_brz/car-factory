package ru.alex_brz.test.used_tests;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.alex_brz.test.product.Product;
import ru.alex_brz.test.test.Test;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;

@Entity(name = "used_tests")
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class UsedTest {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Product idProduct;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Test idTest;

    private static List<String> categories = Arrays.asList("ID", "ID Изделия","ID Участка");

    static List<String> getCategories() {
        return categories;
    }

    public UsedTest(Product idProduct, Test idTest) {
        this.idProduct = idProduct;
        this.idTest = idTest;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
