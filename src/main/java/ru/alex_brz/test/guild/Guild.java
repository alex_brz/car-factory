package ru.alex_brz.test.guild;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.alex_brz.test.area.Area;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity(name = "Guild")
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Guild {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private int idHead;
    private String company;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "idGuild",orphanRemoval = true, cascade = {CascadeType.REMOVE})
    private List<Area> areaList = new ArrayList<>();

    public void addTest(Area test){
        areaList.add(test);
    }

    private static List<String> categories = Arrays.asList("ID", "ID Начальника","Предприятие");

    static List<String> getCategories() {
        return categories;
    }

    public Guild(int idHead, String company) {
        this.idHead = idHead;
        this.company = company;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
