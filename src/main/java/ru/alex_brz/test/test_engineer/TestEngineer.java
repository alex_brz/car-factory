package ru.alex_brz.test.test_engineer;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.alex_brz.test.test.Test;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;

@Entity(name = "TestEngineer")
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class TestEngineer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    private Test idTest;

    private static List<String> categories = Arrays.asList("ID", "ID Испытания");

    static List<String> getCategories() {
        return categories;
    }

    public TestEngineer(Test idTest) {
        this.idTest = idTest;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
