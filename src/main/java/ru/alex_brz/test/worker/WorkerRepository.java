package ru.alex_brz.test.worker;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface WorkerRepository extends CrudRepository<Worker, Integer> {
    List<Worker> findAll();

}