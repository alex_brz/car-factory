package ru.alex_brz.test.test_engineer;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.alex_brz.test.test.Test;
import ru.alex_brz.test.test.TestRepository;

import java.util.Optional;

@Route("testEngineerEditor")
public class TestEngineerEditorView extends AppLayout implements HasUrlParameter<Integer> {

    private Integer id;
    FormLayout contactForm;
    FormLayout buttonLayout;
    private TextField idTestText;
    Button saveContact;
    Button cancel;

    @Autowired
    TestEngineerRepository testEngineerRepository;
    @Autowired
    TestRepository testRepository;

    public TestEngineerEditorView() {
        //Создаем объекты для формы
        contactForm = new FormLayout();
        buttonLayout = new FormLayout();
        idTestText = new TextField(TestEngineer.getCategories().get(1));
        saveContact = new Button("Сохранить");
        cancel = new Button("Отмена");

        saveContact.getElement().getThemeList().add("primary");
        //Добавим все элементы на форму
        buttonLayout.add(saveContact, cancel);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(contactForm);
        verticalLayout.add(buttonLayout);
        setContent(verticalLayout);
    }
    @Transactional
    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer contactId) {
        id = contactId;
        if (!id.equals(0)) {
            addToNavbar(new H3("Редактировать испытателя"));
        } else {
            addToNavbar(new H3("Добавить испытателя"));
        }
        fillForm();
    }


    private void fillForm() {

        if (!id.equals(0)) {
            Optional<TestEngineer> equipment = testEngineerRepository.findById(id);
            equipment.ifPresent(x -> {
                idTestText.setValue(String.valueOf(x.getIdTest()));
            });
        }

        saveContact.addClickListener(clickEvent -> {
            save();
            notificationSave();
        });

        cancel.addClickListener(clickEvent -> {
            notificationCancel();
        });
    }

    private void save(){
        TestEngineer testEngineer = new TestEngineer();
        if (!id.equals(0)) {
            testEngineer.setId(id);
        }
        Test test = testRepository.findById(Integer.parseInt(idTestText.getValue())).orElse(null);
        if(test!=null) {
            testEngineer.setIdTest(test);

            test.addTestEngineer(testEngineer);
            testRepository.save(test);
            testEngineerRepository.save(testEngineer);
        }else{
            notificationError();
        }
    }
    private void notificationSave(){
        Notification notification = new Notification(id.equals(0) ?
                "Испытатель успешно создан" : "Испытатель был изменен", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(TestEngineerListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationCancel(){
        Notification notification = new Notification("Отмена изменений", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(TestEngineerListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationError(){
        Notification notification = new Notification("Ошибка. Несуществующий id", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.open();
    }
}
