package ru.alex_brz.test;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.alex_brz.test.area.Area;
import ru.alex_brz.test.area.AreaListView;
import ru.alex_brz.test.area.AreaRepository;
import ru.alex_brz.test.brigade.Brigade;
import ru.alex_brz.test.brigade.BrigadeListView;
import ru.alex_brz.test.brigade.BrigadeRepository;
import ru.alex_brz.test.characteristic.Characteristic;
import ru.alex_brz.test.characteristic.CharacteristicListView;
import ru.alex_brz.test.characteristic.CharacteristicRepository;
import ru.alex_brz.test.engineer.Engineer;
import ru.alex_brz.test.engineer.EngineerListView;
import ru.alex_brz.test.engineer.EngineerRepository;
import ru.alex_brz.test.equipment.Equipment;
import ru.alex_brz.test.equipment.EquipmentListView;
import ru.alex_brz.test.equipment.EquipmentRepository;
import ru.alex_brz.test.guild.Guild;
import ru.alex_brz.test.guild.GuildListView;
import ru.alex_brz.test.guild.GuildRepository;
import ru.alex_brz.test.laboratory.Laboratory;
import ru.alex_brz.test.laboratory.LaboratoryListView;
import ru.alex_brz.test.laboratory.LaboratoryRepository;
import ru.alex_brz.test.master.Master;
import ru.alex_brz.test.master.MasterListView;
import ru.alex_brz.test.master.MasterRepository;
import ru.alex_brz.test.product.Product;
import ru.alex_brz.test.product.ProductListView;
import ru.alex_brz.test.product.ProductRepository;
import ru.alex_brz.test.test.Test;
import ru.alex_brz.test.test.TestListView;
import ru.alex_brz.test.test.TestRepository;
import ru.alex_brz.test.test_engineer.TestEngineer;
import ru.alex_brz.test.test_engineer.TestEngineerListView;
import ru.alex_brz.test.test_engineer.TestEngineerRepository;
import ru.alex_brz.test.used_areas.UsedArea;
import ru.alex_brz.test.used_areas.UsedAreaListView;
import ru.alex_brz.test.used_areas.UsedAreaRepository;
import ru.alex_brz.test.used_tests.UsedTest;
import ru.alex_brz.test.used_tests.UsedTestListView;
import ru.alex_brz.test.used_tests.UsedTestRepository;
import ru.alex_brz.test.worker.Worker;
import ru.alex_brz.test.worker.WorkerListView;
import ru.alex_brz.test.worker.WorkerRepository;

import java.sql.Time;
import java.util.*;

@Route("")
public class MainView extends AppLayout {

    private VerticalLayout verticalLayoutPeople= new VerticalLayout();
    private VerticalLayout verticalLayoutPlace= new VerticalLayout();
    private VerticalLayout verticalLayoutProduct= new VerticalLayout();
    private VerticalLayout verticalLayoutTest= new VerticalLayout();
    private List<RouterLink> allTablesLinks = new ArrayList<RouterLink>() {};

    public MainView() {
        //allTablesLinks.add(new RouterLink("Контакты", ContactListView.class));
        verticalLayoutPeople.add(new H3("Люди"));
        verticalLayoutPeople.add(new RouterLink("Рабочии", WorkerListView.class));
        verticalLayoutPeople.add(new RouterLink("Инженеры", EngineerListView.class));
        verticalLayoutPeople.add(new RouterLink("Мастера", MasterListView.class));
        verticalLayoutPeople.add(new RouterLink("Бригады", BrigadeListView.class));

        verticalLayoutPlace.add(new H3("Места"));
        verticalLayoutPlace.add(new RouterLink("Цеха", GuildListView.class));
        verticalLayoutPlace.add(new RouterLink("Участки", AreaListView.class));

        verticalLayoutProduct.add(new H3("Изделия"));
        verticalLayoutProduct.add(new RouterLink("Изделия", ProductListView.class));
        verticalLayoutProduct.add(new RouterLink("Характеристики изделий", CharacteristicListView.class));
        verticalLayoutProduct.add(new RouterLink("Участки товара", UsedAreaListView.class));
        verticalLayoutProduct.add(new RouterLink("Испытания товара", UsedTestListView.class));

        verticalLayoutTest.add(new H3("Испытания"));
        verticalLayoutTest.add(new RouterLink("Лаборатории", LaboratoryListView.class));
        verticalLayoutTest.add(new RouterLink("Оборудование", EquipmentListView.class));
        verticalLayoutTest.add(new RouterLink("Испытания", TestListView.class));
        verticalLayoutTest.add(new RouterLink("Испытатели", TestEngineerListView.class));

        HorizontalLayout mainLayout = new HorizontalLayout();
        mainLayout.add(verticalLayoutPeople, verticalLayoutPlace, verticalLayoutProduct, verticalLayoutTest);

        Button btnAddData = new Button("Наполнить таблицы");
        btnAddData.addClickListener(new ComponentEventListener<ClickEvent<Button>>() {
            @Override
            public void onComponentEvent(ClickEvent<Button> buttonClickEvent) {
                addDataToTables();
            }
        });
        Button btnClearData = new Button("Очистить таблицы");
        btnAddData.addClickListener(new ComponentEventListener<ClickEvent<Button>>() {
            @Override
            public void onComponentEvent(ClickEvent<Button> buttonClickEvent) {
                deleteAllTables();
            }
        });

        MenuBar menuBar = new MenuBar();
        menuBar.addItem("Страница запросов");
        menuBar.addItem(btnAddData);
        //menuBar.addItem(btnClearData);
        addToNavbar(new H3("Автомобилестроительное предприятие"));
        addToNavbar(menuBar);
        setContent(mainLayout);
    }

    @Autowired
    AreaRepository areaRepository;
    @Autowired
    BrigadeRepository brigadeRepository;
    @Autowired
    CharacteristicRepository characteristicRepository;
    @Autowired
    EngineerRepository engineerRepository;
    @Autowired
    EquipmentRepository equipmentRepository;
    @Autowired
    GuildRepository guildRepository;
    @Autowired
    LaboratoryRepository laboratoryRepository;
    @Autowired
    MasterRepository masterRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    TestRepository testRepository;
    @Autowired
    TestEngineerRepository testEngineerRepository;
    @Autowired
    UsedAreaRepository usedAreaRepository;
    @Autowired
    UsedTestRepository usedTestRepository;
    @Autowired
    WorkerRepository workerRepository;

    @Transactional
    void deleteAllTables(){
        areaRepository.deleteAll();
        brigadeRepository.deleteAll();
        characteristicRepository.deleteAll();
        engineerRepository.deleteAll();
        equipmentRepository.deleteAll();
        guildRepository.deleteAll();
        laboratoryRepository.deleteAll();
        masterRepository.deleteAll();
        productRepository.deleteAll();
        testRepository.deleteAll();
        testEngineerRepository.deleteAll();
        usedAreaRepository.deleteAll();
        usedTestRepository.deleteAll();
        workerRepository.deleteAll();
    }

    @Transactional
    void addDataToTables() {

        Guild guild1 = new Guild(1, "Toyota");
        Guild guild2 = new Guild(2, "Toyota");
        Guild guild3 = new Guild(3, "Toyota");
        Guild guild4 = new Guild(4, "NISSAN");
        Guild guild5 = new Guild(5, "NISSAN");

        Set<Guild> guildSet = new HashSet<Guild>();
        guildSet.add(guild1);
        guildSet.add(guild2);
        guildSet.add(guild3);
        guildSet.add(guild4);
        guildSet.add(guild5);

        guildRepository.saveAll(guildSet);

        List<Engineer> engineerList = Arrays.asList(
            new Engineer("Alex", "ColorEngineer"),
            new Engineer("Jack", "Технолог"),
            new Engineer("Александр", "Инженер"),
            new Engineer("Иван", "Младший инженер"),
            new Engineer("Иван II", "Инженер"),
            new Engineer("Иван III", "Моторист"),
            new Engineer("Иван Грозный", "Старший инженер"),
            new Engineer("Петр", "Техник"),
            new Engineer("Павел", "Настройщик")
        );

        engineerRepository.saveAll(engineerList);

        Product product1 = new Product("RAV 4", "Легковые", new Time(12,10,0),Time.valueOf("13:00:00"));
        Product product2 = new Product("Camry", "Легковые", new Time(12,10,0),Time.valueOf("13:00:00"));
        Product product3 = new Product("Alphard", "Автобусы", new Time(12,10,0),Time.valueOf("13:00:00"));
        Product product4 = new Product("IDK", "Грузовые", new Time(12,10,0),Time.valueOf("13:00:00"));

        productRepository.save(product1);
        productRepository.save(product2);
        productRepository.save(product3);
        productRepository.save(product4);

        List<Characteristic> characteristicList = Arrays.asList(
                new Characteristic("Легковые", "Макс. Скорость 180"),
                new Characteristic("Легковые", "Мощность 150"),
                new Characteristic("Автобусы", "Вместительность 50"),
                new Characteristic("Грузовые", "Грузоподъемность 10т")
        );
        characteristicRepository.saveAll(characteristicList);

        List<Area> areaList = Arrays.asList(
                new Area(guild1, engineerList.get(4), product1),
                new Area(guild1, engineerList.get(3), null),
                new Area(guild2, engineerList.get(4), product2),
                new Area(guild2, engineerList.get(1), null),
                new Area(guild3, engineerList.get(2), product3),
                new Area(guild1, engineerList.get(3), product4),
                new Area(guild3, engineerList.get(1), null));
        areaRepository.saveAll(areaList);

        List<Master> masterList = Arrays.asList(
                new Master(engineerList.get(0), engineerList.get(4)),
                new Master(engineerList.get(1), engineerList.get(4)),
                new Master(engineerList.get(5), engineerList.get(3)),
                new Master(engineerList.get(6), engineerList.get(3)),
                new Master(engineerList.get(7), engineerList.get(2)),
                new Master(engineerList.get(8), engineerList.get(2))
        );
        masterRepository.saveAll(masterList);

        List<Worker> workerList = Arrays.asList(
                new Worker("Маляр"),
                new Worker("Маляр"),
                new Worker("Грузчик"),
                new Worker("Сборщик"),
                new Worker("Сварщик"),
                new Worker("Слесарь"),
                new Worker("Стажер")
        );
        workerRepository.saveAll(workerList);

        List<Brigade> brigadeList = Arrays.asList(
                new Brigade(areaList.get(0), workerList.get(0), product1),
                new Brigade(areaList.get(1), workerList.get(4), product2),
                new Brigade(areaList.get(2), workerList.get(3), product3)
        );
        brigadeRepository.saveAll(brigadeList);

        List<Worker> list = workerRepository.findAll();
        list.get(0).setIdBrigade(brigadeList.get(0));
        list.get(1).setIdBrigade(brigadeList.get(0));
        list.get(2).setIdBrigade(brigadeList.get(2));
        list.get(3).setIdBrigade(brigadeList.get(1));
        list.get(4).setIdBrigade(brigadeList.get(1));
        list.get(5).setIdBrigade(brigadeList.get(2));
        list.get(6).setIdBrigade(brigadeList.get(1));
        workerRepository.saveAll(list);

        List<UsedArea> usedAreaList = Arrays.asList(
                new UsedArea(product1, areaList.get(0)),
                new UsedArea(product1, areaList.get(1)),
                new UsedArea(product1, areaList.get(2)),
                new UsedArea(product2, areaList.get(0)),
                new UsedArea(product2, areaList.get(3)),
                new UsedArea(product3, areaList.get(4)),
                new UsedArea(product3, areaList.get(5)),
                new UsedArea(product4, areaList.get(6)),
                new UsedArea(product4, areaList.get(5))
        );
        usedAreaRepository.saveAll(usedAreaList);

        List<Laboratory> laboratoryList = Arrays.asList(
                new Laboratory(),
                new Laboratory(),
                new Laboratory(),
                new Laboratory()
        );
        laboratoryRepository.saveAll(laboratoryList);

        List<Test> testList = Arrays.asList(
                new Test(laboratoryList.get(0), Time.valueOf("13:00:00")),
                new Test(laboratoryList.get(0), Time.valueOf("14:00:00")),
                new Test(laboratoryList.get(1), Time.valueOf("15:00:00")),
                new Test(laboratoryList.get(2), Time.valueOf("10:00:00")),
                new Test(laboratoryList.get(3), Time.valueOf("19:00:00"))
        );
        testRepository.saveAll(testList);

        List<TestEngineer> testEngineerList = Arrays.asList(
                new TestEngineer(testList.get(0)),
                new TestEngineer(testList.get(1)),
                new TestEngineer(testList.get(1)),
                new TestEngineer(testList.get(2)),
                new TestEngineer(testList.get(3))
        );
        testEngineerRepository.saveAll(testEngineerList);

        List<Equipment> equipmentList = Arrays.asList(
                new Equipment(testList.get(0), "Станок 1"),
                new Equipment(testList.get(1), "Станок 2"),
                new Equipment(testList.get(2), "Станок 3"),
                new Equipment(testList.get(0), "Super Tester 1000"),
                new Equipment(testList.get(4), "Super Tester 1500"),
                new Equipment(testList.get(3), "CAT"),
                new Equipment(testList.get(3), "Комната для допросов?"),
                new Equipment(testList.get(1), "Top secret")
        );
        equipmentRepository.saveAll(equipmentList);

        List<UsedTest> usedTestList = Arrays.asList(
                new UsedTest(product1, testList.get(0)),
                new UsedTest(product1, testList.get(1)),
                new UsedTest(product1, testList.get(2)),
                new UsedTest(product2, testList.get(0)),
                new UsedTest(product2, testList.get(1)),
                new UsedTest(product2, testList.get(3)),
                new UsedTest(product2, testList.get(4)),
                new UsedTest(product3, testList.get(4)),
                new UsedTest(product3, testList.get(0)),
                new UsedTest(product4, testList.get(3)),
                new UsedTest(product4, testList.get(2))
        );
        usedTestRepository.saveAll(usedTestList);

    }
}
