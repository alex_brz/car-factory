package ru.alex_brz.test.contact;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Route("contactEditor")
public class ContactEditorView extends AppLayout implements HasUrlParameter<Integer> {

    Integer id;
    FormLayout contactForm;
    FormLayout buttonLayout;
    TextField firstName;
    TextField secondName;
    TextField fatherName;
    TextField numberPhone;
    TextField email;
    Button saveContact;
    Button cancel;

    @Autowired
    ContactRepository contactRepository;

    /*Binder<Contact> binder = new Binder<>(Contact.class);
    @Setter
    private ChangeHandler changeHandler;

    public interface ChangeHandler{
        void onChange();
    }*/

    public ContactEditorView() {
        //Создаем объекты для формы
        contactForm = new FormLayout();
        buttonLayout = new FormLayout();
        firstName = new TextField("Имя");
        secondName = new TextField("Фамилия");
        fatherName = new TextField("Отчество");
        numberPhone = new TextField("Номер телефона");
        email = new TextField("Электронная почта");
        saveContact = new Button("Сохранить");
        cancel = new Button("Отмена");

        saveContact.getElement().getThemeList().add("primary");
        //Добавим все элементы на форму
        buttonLayout.add(saveContact, cancel);
        contactForm.add(firstName, secondName, fatherName, numberPhone, email);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(contactForm);
        verticalLayout.add(buttonLayout);
        setContent(verticalLayout);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer contactId) {
        id = contactId;
        if (!id.equals(0)) {
            addToNavbar(new H3("Редактирование контакта"));
        } else {
            addToNavbar(new H3("Создание контакта"));
        }
        fillForm();
    }


    private void fillForm() {

        if (!id.equals(0)) {
            Optional<Contact> contact = contactRepository.findById(id);
            contact.ifPresent(x -> {
                firstName.setValue(x.getFirstName());
                secondName.setValue(x.getSecondName());
                fatherName.setValue(x.getFatherName());
                numberPhone.setValue(x.getNumberPhone());
                email.setValue(x.getEmail());
            });
        }

        saveContact.addClickListener(clickEvent -> {
            save();
            notificationSave();
        });

        cancel.addClickListener(clickEvent -> {
            notificationCancel();
        });
    }

    private void save(){
        Contact contact = new Contact();
        if (!id.equals(0)) {
            contact.setId(id);
        }
        contact.setFirstName(firstName.getValue());
        contact.setSecondName(secondName.getValue());
        contact.setFatherName(fatherName.getValue());
        contact.setEmail(email.getValue());
        contact.setNumberPhone(numberPhone.getValue());
        contactRepository.save(contact);
    }
    private void notificationSave(){
        Notification notification = new Notification(id.equals(0) ? "Контакт успешно создан" : "Контакт был изменен", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(ContactListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
    private void notificationCancel(){
        Notification notification = new Notification("Отмена изменений", 1000);
        notification.setPosition(Notification.Position.MIDDLE);
        notification.addDetachListener(detachEvent -> {
            UI.getCurrent().navigate(ContactListView.class);
        });
        contactForm.setEnabled(false);
        notification.open();
    }
}
