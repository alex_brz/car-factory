package ru.alex_brz.test.brigade;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BrigadeRepository extends CrudRepository<Brigade, Integer> {
    List<Brigade> findAll();

}