package ru.alex_brz.test.guild;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GuildRepository extends CrudRepository<Guild, Integer> {
    List<Guild> findAll();

}