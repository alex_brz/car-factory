package ru.alex_brz.test.engineer;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.NativeButtonRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.beans.factory.annotation.Autowired;
import ru.alex_brz.test.MainView;

import javax.annotation.PostConstruct;
import java.util.List;

@Route("engineers")
public class EngineerListView extends AppLayout {

    private VerticalLayout layout;
    private Grid<Engineer> grid;
    private RouterLink linkCreate;
    private TextField filter = new TextField();
    private Button buttonDeleteTable = new Button("Удалить таблицу");
    private ComboBox<String> categoryComboBox = new ComboBox<>();

    @Autowired
    EngineerRepository engineerRepository;

    public EngineerListView() {
        layout = new VerticalLayout();
        grid = new Grid<>();
        linkCreate = new RouterLink("Добавить инженера", EngineerEditorView.class, 0);
        layout.add(linkCreate);

        HorizontalLayout filterLayout = new HorizontalLayout();
        filterLayout.add(categoryComboBox);
        filterLayout.add(filter);

        layout.add(filterLayout);
        layout.add(grid);
        addToNavbar(new H3("Список инженеров"));
        MenuBar menuBar = new MenuBar();
        menuBar.addItem("Hello!");
        menuBar.addItem(new RouterLink("Main page", MainView.class));
        addToNavbar(menuBar);
        addToNavbar(buttonDeleteTable);
        setContent(layout);
    }

    @PostConstruct
    public void fillGrid() {
        buttonDeleteTable.addClickListener(buttonClickEvent -> {
            Dialog dialog = new Dialog();
            Button confirm = new Button("Удалить", VaadinIcon.TRASH.create());
            confirm.getElement().getThemeList().add("error");
            Button cancel = new Button("Отмена", VaadinIcon.CLOSE_CIRCLE.create());
            dialog.add("Вы уверены что хотите удалить таблицу?");
            dialog.add(confirm);
            dialog.add(cancel);

            confirm.addClickListener(clickEvent -> {
                engineerRepository.deleteAll();
                dialog.close();
                Notification notification = new Notification("Таблица очищена", 1000);
                notification.setPosition(Notification.Position.MIDDLE);
                notification.open();

                grid.setItems(engineerRepository.findAll());

            });

            cancel.addClickListener(clickEvent -> {
                dialog.close();
            });

            dialog.open();

        });

        categoryComboBox.setPlaceholder("Choose category");
        categoryComboBox.setItems(Engineer.getCategories());
        categoryComboBox.addValueChangeListener(category -> filterList(filter.getValue()));

        filter.setPlaceholder("Type to filter");
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(field -> filterList(field.getValue()));

        List<Engineer> engineers = engineerRepository.findAll();
        if (!engineers.isEmpty()) {

            //Выведем столбцы в нужном порядке
            grid.addColumn(Engineer::getId).setHeader("ID");
            grid.addColumn(Engineer::getName).setHeader("Имя");
            grid.addColumn(Engineer::getProfession).setHeader("Профессия");
            //Добавим кнопку удаления и редактирования
            grid.addColumn(new NativeButtonRenderer<>("Редактировать", engineer -> {
                UI.getCurrent().navigate(EngineerEditorView.class, engineer.getId());
            }));
            grid.addColumn(new NativeButtonRenderer<>("Удалить", engineer -> {
                Dialog dialog = new Dialog();
                Button confirm = new Button("Удалить", VaadinIcon.TRASH.create());
                confirm.getElement().getThemeList().add("error");
                Button cancel = new Button("Отмена", VaadinIcon.CLOSE_CIRCLE.create());
                dialog.add("Вы уверены что хотите удалить запись?");
                dialog.add(confirm);
                dialog.add(cancel);

                confirm.addClickListener(clickEvent -> {
                    engineerRepository.delete(engineer);
                    dialog.close();
                    Notification notification = new Notification("Контакт удален", 1000);
                    notification.setPosition(Notification.Position.MIDDLE);
                    notification.open();

                    grid.setItems(engineerRepository.findAll());

                });

                cancel.addClickListener(clickEvent -> {
                    dialog.close();
                });

                dialog.open();

            }));

            for (Grid.Column column : grid.getColumns()) {
                column.setSortable(true);
            }
            grid.setItems(engineers);

        }
    }

    private void filterList(String value){
        /*int i = Contact.getCategories().indexOf(categoryComboBox.getValue());
        switch (i){
            case 0:
                grid.setItems(contactRepository.findContactsById(Integer.parseInt(value)));
                break;
            case 1: grid.setItems(contactRepository.findContactsByFirstNameIsContaining(value));
                break;
            case 2: grid.setItems(contactRepository.findContactsBySecondNameIsContaining(value));
                break;
            case 3: grid.setItems(contactRepository.findContactsByFatherNameIsContaining(value));
                break;
            case 4: grid.setItems(contactRepository.findContactsByNumberPhoneIsContaining(value));
                break;
            case 5: grid.setItems(contactRepository.findContactsByEmailIsContaining(value));
                break;
        }*/
    }


}
